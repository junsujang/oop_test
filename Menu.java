import com.sun.istack.internal.NotNull;

import java.util.List;
interface IMenu{
    MenuItem choose(String name);
}
public class Menu implements IMenu{

    private List<MenuItem> items;

    public Menu(List<MenuItem> items){
        this.items=items;
    }

    @Override
    public MenuItem choose(@NotNull String name) {
        for(MenuItem menuItem:items){
            if(menuItem.getName().equals(name)){
                return menuItem;
            }
        }

        return null;
    }
}
