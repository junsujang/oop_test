import java.util.ArrayList;
import java.util.List;

/**
 * 협력(=특정 목표)
 * -> 손님이 커피를 주문하여 받아간다.
 *
 * 역할:책임
 * -> 손님:커피를 주문할 책임,
 * -> 메뉴판:메뉴항목을 보여줄 책임,
 * -> 바리스타:커피를 제조할 책임,
 * -> 커피:주문된 커피의 정보를 알려줄 책임
 */
public class Test {
    public static void main(String[] args){
        Customer customer = new Customer();
        Barista barista = new Barista();

        List<MenuItem> menulist = new ArrayList<>();
        menulist.add(new MenuItem("아메리카노",1500));
        menulist.add(new MenuItem("바닐라 라떼",3500));
        menulist.add(new MenuItem("카라멜 마끼야또",3500));
        menulist.add(new MenuItem("레몬에이드",2500));

        Menu menu = new Menu(menulist);
        customer.order("레몬에이",menu,barista);

    }
}
