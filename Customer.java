import com.sun.istack.internal.NotNull;

interface ICustomer{
    void order(String menuName,Menu menu,Barista barista);
}

public class Customer implements ICustomer{

    @Override
    public void order(@NotNull String menuName, @NotNull Menu menu, @NotNull Barista barista) {
        MenuItem menuItem = null;
        Coffee coffee = null;

        try {
            menuItem = menu.choose(menuName);
            coffee = barista.makeCoffee(menuItem);
            System.out.println(coffee.toString());
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(menuName+"-> 주문하신 메뉴명은 존재하지 않습니다.");
        }
        return;
    }
}
