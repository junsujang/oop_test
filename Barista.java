import com.sun.istack.internal.NotNull;

interface IBarista{
    Coffee makeCoffee(MenuItem menuItem);
}

public class Barista implements IBarista{

    @Override
    public Coffee makeCoffee(@NotNull MenuItem menuItem) {
        Coffee coffee = new Coffee(menuItem);
        return coffee;
    }
}
